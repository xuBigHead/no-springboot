package com.xmm.demo.assist.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author xmm
 */
@Data
public class ComplicatedBean {
    Long id;
    ArrayList<SimpleBean> simpleBeanList;
    HashSet<SimpleBean> simpleBeanSet;
    HashMap<String,SimpleBean> simpleBeanMap;
}
