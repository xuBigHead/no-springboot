package com.xmm.demo.creational.singleton;

/**
 * 通过静态内部类的方式来实现单例模式
 * 线程安全，调用效率高，可以延时加载
 *
 * @author xmm
 * @since 2020/1/20
 */
public class SingletonStaticInnerClazz {
    private static SingletonStaticInnerClazz instance = null;
    private SingletonStaticInnerClazz() {
    }

    /**
     * 使用一个内部类来维护单例
     */
    private static class SingletonFactory{
        private static SingletonStaticInnerClazz instance = new SingletonStaticInnerClazz();
    }
    public static SingletonStaticInnerClazz getInstance() {
        return SingletonFactory.instance;
    }
    public Object readResolve() {
        return instance;
    }
}
