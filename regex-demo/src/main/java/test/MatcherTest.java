package test;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmm
 * @since 2020/1/20
 */
public class MatcherTest {
    /**
     * pattern()
     *  返回创建Matcher的Pattern对象
     */
    @Test
    public void pattern(){
        String regex = "\\d+";
        Pattern pattern1 = Pattern.compile(regex);
        Matcher matcher = pattern1.matcher("123");
        Pattern pattern2 = matcher.pattern();
        System.err.println(pattern1 == pattern2);
    }

    /**
     * matches()是整个匹配且总是从头开始
     *
     * matches()
     *  根据指定的regex来匹配string，等价于Pattern.matcher()方法
     */
    @Test
    public void matches(){
        String regex = "\\d+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("123");
        boolean b = matcher.matches();
        System.err.println(b);
    }

    /**
     * lookingAt()
     *  对string头部进行匹配，只要匹配到就返回true
     */
    @Test
    public void lookingAt(){
        String regex = "\\d+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("1a1aaa");
        System.err.println(matcher.lookingAt()  + " 匹配结果为：" + matcher.group());
        System.err.println(matcher.lookingAt()  + " 匹配结果为：" + matcher.group());
    }

    /**
     * find()是部分匹配且从上一次匹配结束时开始找
     *
     * find()
     *  对string进行匹配，从下标为0的位置开始，任意位置匹配到即返回true
     * find(int i)
     *  对string进行匹配，从下标为i的位置开始，任意位置匹配到即返回true
     */
    @Test
    public void find(){
        String regex = "\\d+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("a1a2a3a");
        System.err.println(matcher.find() + " 匹配结果为：" + matcher.group());
        System.err.println(matcher.find() + " 匹配结果为：" + matcher.group());
        matcher.reset();
        System.err.println(matcher.find(4) + " 匹配结果为：" + matcher.group());
    }

    /**
     * 使用Matcher的matches()|lookingAt()|find()方法后可以使用start()|end()|group()得到更详细信息
     * start()
     *  返回匹配到的子字符串在字符串中的索引位置
     * end()
     *  返回匹配到的子字符串的最后一个字符在字符串中的索引位置 + 1
     * group()
     *  返回匹配到的子字符串
     */
    @Test
    public void startAndEndAndGroup(){
        String regex = "\\d+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher=pattern.matcher("aa222333bbb");
        System.err.println(matcher.find());
        System.err.println(matcher.start());
        System.err.println(matcher.end());
        System.err.println(matcher.group());
    }

    /**
     * start(int group)
     *  返回指定分组匹配到的子字符串在字符串中的索引位置
     * end(int group)
     *  返回指定分组匹配到的子字符串的最后一个字符在字符串中的索引位置 + 1
     * group(int group)
     *  返回指定分组匹配到的子字符串
     */
    @Test
    public void group() {
        String regex = "(([abc]+)([123]+))([-_%]+)";
        Pattern pattern = Pattern.compile(regex);
        //体会一下组的用法，组的顺序，只数"("，第一个为第一组，第二个是第二组，第0组表示整个表达式
        String str = "abc112233%_%_-";
        System.err.println(str.length());
        Matcher matcher = pattern.matcher(str);
        System.out.println(matcher.matches());
        System.out.println(matcher.group());
        System.out.println(matcher.group(0));
        System.out.println(matcher.groupCount());
        System.out.println("组1：" + matcher.group(1));
        System.out.println("组2：" + matcher.group(2));
        System.out.println("组3：" + matcher.group(3));
        System.out.println("组4：" + matcher.group(4));
        System.out.println(matcher.end());
        System.out.println(matcher.end(2));
        System.out.println(matcher.start());
        System.out.println(matcher.start(2));
    }

    /**
     * groupCount()
     *  返回匹配到的string的组数
     */
    @Test
    public void groupCount(){
        String regex = "([a-z]+)(\\d+)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher=pattern.matcher("aac333bbb");
        System.err.println(matcher.groupCount());
    }

    @Test
    public void appendReplacement() {
        String regex = "cat";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("one cat two cats in the yard");
        StringBuffer sb = new StringBuffer();
        boolean result = matcher.find();
        int i = 1;
        System.out.println("one cat two cats in the yard");
        while (result) {
            matcher.appendReplacement(sb, "dog");
            System.out.println(matcher.group());
            System.out.println("第" + (i++) + "次:" + sb.toString());
            result = matcher.find();
        }
        System.out.println(sb.toString());
        matcher.appendTail(sb);
        System.out.println(sb.toString());
    }
}
