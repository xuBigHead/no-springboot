package com.xmm.demo.creational.singleton;

/**
 * 实现单例模式的最佳方法，更简洁，自动支持序列化机制，绝对防止多次实例化。
 *
 * @author xmm
 * @since 2020/1/20
 */
public enum SingletonEnum {
    INSTANCE;
}
