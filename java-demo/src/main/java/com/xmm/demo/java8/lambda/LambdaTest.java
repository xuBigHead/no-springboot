package com.xmm.demo.java8.lambda;

import com.xmm.demo.java8.lambda.interfaces.ArgsReturn;
import com.xmm.demo.java8.lambda.interfaces.NoArgsNoReturn;
import com.xmm.demo.java8.lambda.interfaces.ArgsNoReturn;
import com.xmm.demo.java8.lambda.interfaces.NoArgsReturn;
import org.junit.Test;

import java.util.Random;

/**
 * @author xmm
 */
public class LambdaTest {
    /**
     * 无参无返回值
     */
    @Test
    public void noArgsNoReturnDemo(){
        NoArgsNoReturn object = () -> {
            int a = 10,b = 20;
            System.out.println(a + b);
        };
        object.start();
    }
    /**
     * 有参无返回值
     */
    @Test
    public void argsNoReturnDemo(){
        ArgsNoReturn object = (a, b) -> System.out.println(a + b);
        object.start("Hello ","World!!!");
    }
    /**
     * 无参有返回值
     */
    @Test
    public void noArgsReturnDemo(){
        NoArgsReturn object = () -> {
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                int randomNum = new Random().nextInt(10);
                sum += randomNum;
            }
            return sum + "";
        };
        String s = object.returnStart();
        System.out.println(s);
    }
    /**
     * 有参有返回值
     */
    @Test
    public void argsReturnDemo(){
        ArgsReturn object = (x, y) -> x +" " + y;
        String string = object.returnStart("hello", "world!!");
        System.out.println(string);
    }
}

