package com.xmm.demo.java8.stream;

import com.xmm.demo.assist.constant.CommonConstant;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Stream的终止操作
 *
 * @author xmm
 */
public class StreamTerminalApiTest {
    private List<Integer> numList = CommonConstant.integerList;

    @Test
    public void statistics() {
        int count = numList.size();
        int sum = numList.stream().mapToInt(x -> x).sum();
        Double avg = numList.stream()
                .collect(Collectors.averagingInt(Integer::intValue));
        Integer max = numList.stream().max(Integer::compareTo).orElse(0);
        Integer min = numList.stream().min(Integer::compareTo).orElse(0);
        System.out.println(count + ":" + sum + ":" + avg + ":" + max + ":" + min);
    }

    @Test
    public void find() {
        // findFirst()返回非空集合中的第一个值，通常与filter方法结合起来使用
        Integer firstNumber = numList.stream().findFirst().orElse(0);
        // findAny()只要找到任何一个匹配的元素就返回，在并行流时十分有效
        Integer randomNumber = numList.parallelStream().findAny().orElse(0);
        System.out.println(firstNumber + ":" + randomNumber);
    }

    @Test
    public void match() {
        // anyMatch()可以判定集合中是否还有匹配的元素
        boolean isHas = numList.parallelStream().anyMatch(i -> i > 8);
        // allMatch()在所有元素匹配时返回true
        boolean allHas = numList.parallelStream().allMatch(i -> i > 8);
        // noMatch()在没有元素匹配时返回true
        boolean noHas = numList.parallelStream().noneMatch(i -> i > 8);
        System.out.println("存在该元素：" + isHas + "\n"
                + "所有元素符合该条件：" + allHas + "\n"
                + "所有元素都不符合该条件：" + noHas);
    }

    @Test
    public void reduce() {
        //求和
        Integer sum = numList.stream().reduce(Integer::sum).orElse(0);
        System.out.println("sum:" + sum);
        //含有初始标识的，求和
        sum = numList.stream().reduce(10, Integer::sum);
        System.out.println("sum:" + sum);
        //对元素的长度进行求和，类似于一个累加器，会被重复调用)
        sum = numList.stream()
                .reduce(0, (total, y) -> total + y.toString().length(), Integer::sum);
        System.out.println("sum:" + sum);
        //简化一下，对元素长度进行求和。
        sum = numList.stream().map(Objects::toString).mapToInt(String::length).sum();
        System.out.println("sum:" + sum);
    }
}
