package com.xmm.demo.creational.factory;

/**
 * @author xmm
 * @since 2020/1/19
 */
public interface Sender {
    void send();
}
