package com.xmm.demo.io;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Objects;

/**
 * 修改指定目录下文件名
 * @author xmm
 */
@Slf4j
public class FileTest {
    private static final String FILES_NOT_EXIST = "文件集不存在";
    private static final String FILE_NOT_EXIST = "文件不存在";
    public static void main(String[] args) {
        File dir = new File("E:/IT/新建文件夹");
        log.info("[" + dir.getAbsolutePath() + "]是文件夹:" + dir.isDirectory());
        File[] files = dir.listFiles();
        for (File file : Objects.requireNonNull(files,FILES_NOT_EXIST)) {
            log.info(Objects.requireNonNull(file,FILE_NOT_EXIST).getName());
            if(file.isFile()) {
                String newFileName = file.getName()
                        .replace(" ", "")
                        .substring(file.getName().indexOf(".") + 1);
                log.info(newFileName);
                file.renameTo(new File("E:/IT/新建文件夹/" + newFileName));
            }
        }
    }

}
