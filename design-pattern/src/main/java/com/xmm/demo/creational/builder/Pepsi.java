package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public class Pepsi extends BaseColdDrink {
    @Override
    public float price() { return 35.0f; }
    @Override
    public String name() { return "Pepsi"; }
}
