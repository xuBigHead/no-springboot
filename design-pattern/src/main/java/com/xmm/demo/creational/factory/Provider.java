package com.xmm.demo.creational.factory;

/**
 * 一般工厂模式违背了闭包原则，想要扩展程序必须对工厂类进行修改
 * 工厂类可以通过接口来实现，可以通过创建新的工厂类来扩展程序
 *
 * @author xmm
 * @since 2020/1/19
 */
public interface Provider {
    Sender produce();
}
