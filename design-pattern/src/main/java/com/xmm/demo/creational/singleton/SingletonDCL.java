package com.xmm.demo.creational.singleton;

/**
 * 双检锁懒汉式单例模式
 * 由于JVM底层内部模型原因，偶尔会出问题，不建议使用
 *
 * @author xmm
 * @since 2020/1/20
 */
public class SingletonDCL {
    /**
     * 持有私有静态实例，防止被引用，此处赋值为null，目的是实现延时加载
     */
    private static SingletonDCL instance = null;

    /**
     * 私有构造方法，，防止被实例化
     */
    private SingletonDCL() {
    }

    /**
     * 静态工程方法，创建实例，此时毫无线程安全保护，因此要添加synchronized
     * @return      单例对象
     */
    //
    public static SingletonDCL getInstance() {
        if(instance == null) {
            synchronized(instance) {
                if(instance == null) {
                    instance = new SingletonDCL();
                }
            }
        }
        return instance;
    }
}

