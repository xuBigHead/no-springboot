package com.xmm.demo.collection.set;

import java.util.HashSet;
import java.util.Set;

/**
 * @author xmm
 */
public class HashSetTest {
    public static void main(String[] args) {
        Set<String> phones = new HashSet<>(10);
        System.err.println(phones.add("1"));
        System.err.println(phones.add("2"));
        System.err.println(phones.add("1"));
        System.err.println(phones.add("2"));
        System.err.println(phones.add("3"));
        System.err.println(phones);
    }
}
