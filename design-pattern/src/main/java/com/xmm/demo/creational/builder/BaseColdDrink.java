package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public abstract class BaseColdDrink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }
    @Override
    public abstract float price();
}
