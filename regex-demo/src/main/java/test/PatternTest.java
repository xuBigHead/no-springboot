package test;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmm
 * @since 2020/1/20
 */
public class PatternTest {
    private String regex = "\\d+";
    /**
     * compile(String regex)
     *  编译传入的regex
     * compile(String regex, int flags)
     *  以指定模式编译传入的regex
     */
    @Test
    public void compile(){
        Pattern pattern = Pattern.compile(regex);
        System.out.println(pattern.pattern());
        // 匹配时忽略大小写
        Pattern pattern2 = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        System.out.println(pattern2.pattern() + pattern2.flags());
        Pattern pattern3 = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNIX_LINES);
        System.out.println(pattern3.pattern() + pattern3.flags());
    }

    /**
     * flags()
     *  返回当前Pattern的匹配flag参数，flag参数用来控制正则表达式的匹配行为
     */
    @Test
    public void flags(){
        Pattern pattern = Pattern.compile(regex, Pattern.UNIX_LINES);
        int flags = pattern.flags();
        System.out.println(flags);
    }

    /**
     * pattern()
     *  返回compile的参数
     */
    @Test
    public void pattern(){
        Pattern pattern = Pattern.compile(regex);
        String regexStr = pattern.pattern();
        System.err.println(regexStr);
    }

    /**
     * pattern.split(CharSequence input)
     *  根据指定的regex来分割string，并返回string数组，数组长度不限
     * pattern.split(CharSequence input, int limit)
     *  根据指定的regex来分割string，并返回string数组，数组长度不能超过limit
     */
    @Test
    public void spilt(){
        Pattern pattern = Pattern.compile(regex);
        String[] strArrays = pattern.split("h1e2l3l4o");
        for (String str : strArrays) {
            System.err.println("no limit : " + str);
        }
        String[] strArrays2 = pattern.split("h1e2l3l4o", 2);
        for (String str : strArrays2) {
            System.err.println("limit : " + str);
        }
    }

    /**
     * Pattern.matches(String regex, CharSequence input)
     *  快速匹配string，适用于只匹配一次，且匹配全部string
     */
    @Test
    public void matcher(){
        boolean matches = Pattern.matches(regex, "1234a");
        System.err.println(matches);
    }

    @Test
    public void patternUnixLines() {
        String regex = ".";
        Pattern patternUnixLines1 = Pattern.compile(regex, Pattern.UNIX_LINES);
        Pattern patternUnixLines2 = Pattern.compile("(?d)" + regex);
        Pattern patternUnixLines3 = Pattern.compile(regex);
        patternModel("\n\r",patternUnixLines1,patternUnixLines2,patternUnixLines3);
    }

    @Test
    public void patternDotall() {
        String regex = ".";
        Pattern patternDotall1 = Pattern.compile(regex, Pattern.DOTALL);
        Pattern patternDotall2 = Pattern.compile("(?s)" + regex);
        Pattern patternDotall3 = Pattern.compile(regex);
        patternModel("\n\r",patternDotall1,patternDotall2,patternDotall3);
    }

    @Test
    public void patternCaseInsensitive() {
        String regex = "a";
        Pattern patternCaseInsensitive1 = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Pattern patternCaseInsensitive2 = Pattern.compile("(?i)" + regex);
        Pattern patternCaseInsensitive3 = Pattern.compile(regex);
        patternModel("aA",patternCaseInsensitive1,patternCaseInsensitive2,patternCaseInsensitive3);
    }

    @Test
    public void patternComments() {
        String regex = " a a #ccc";
        Pattern patternComments1 = Pattern.compile(regex, Pattern.COMMENTS);
        Pattern patternComments2 = Pattern.compile("(?x)" + regex);
        patternModel("aa a a #ccc",patternComments1,patternComments2);
    }

    private Pattern patternMultiline1 = Pattern.compile("^.?", Pattern.MULTILINE | Pattern.DOTALL);
    private Pattern patternMultiline2 = Pattern.compile("(?m)^hell.*oo$", Pattern.DOTALL);
    private Pattern patternMultiline3 = Pattern.compile("^hell(.|[^.])*oo$");

    @Test
    public void patternMultiline() {
        Matcher matcher1 = patternMultiline1.matcher("helloohelloo,loveroo");
        System.out.println(matcher1.find());
        System.out.println(matcher1.group());
        System.out.println("start:" + matcher1.start() + "end:" + matcher1.end());
        System.out.println(matcher1.find());

        Matcher matcher2 = patternMultiline2.matcher("hello,Worldoo\nhello,loveroo");
        System.out.println(matcher2.find());
        System.out.println("start:" + matcher2.start() + "end:" + matcher2.end());
        System.out.println(matcher2.find());

        Matcher matcher3 = patternMultiline3.matcher("hello,Worldoo\nhello,loveroo");
        System.out.println(matcher3.find());
        System.out.println("start:" + matcher3.start() + "end:" + matcher3.end());
        System.out.println(matcher3.find());
    }

    private Pattern patternCanonEq = Pattern.compile("a\u030A", Pattern.CANON_EQ);
    @Test
    public void patternCanonEq(){
        System.out.println(Character.getType('\u030A'));
        System.out.println(Character.isISOControl('\u030A'));
        System.out.println(Character.isUnicodeIdentifierPart('\u030A'));
        System.out.println(Character.getType('\u00E5'));
        System.out.println(Character.isISOControl('\u00E5'));
        System.out.println(Character.isUnicodeIdentifierPart('\u00E5'));
        Matcher matcher = patternCanonEq.matcher("\u00E5");
        System.out.println(matcher.matches());
        System.out.println(Character.getType('\u0085'));
        System.out.println(Character.isISOControl('\u0085'));
        System.out.println(Character.isUnicodeIdentifierPart('\u0085'));
    }

    private void patternModel(String str, Pattern... patterns) {
        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(str);
            System.out.println(matcher.find());
            System.out.println(matcher.find());
        }
    }
}