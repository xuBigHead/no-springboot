package constant;

/**
 * 常用正则表达式
 */
public interface RegexConstant {
    /**
     * 用户名
     */
    String USER_NAME = "^[a-zA-Z\\u4E00-\\u9FA5][a-zA-Z0-9_\\u4E00-\\u9FA5]{1,11}$";

    /**
     * 密码
     */
    String USER_PASSWORD = "^.{6,32}$";

    /**
     * 邮箱
     */
    String EMAIL = "^\\w+([-+.]*\\w+)*@([\\da-z](-[\\da-z])?)+(\\.{1,2}[a-z]+)+$";

    /**
     * 手机号
     */
    String PHONE = "^1[3456789]\\d{9}$";

    /**
     * 手机号或者邮箱
     */
    String EMAIL_OR_PHONE = EMAIL + "|" + PHONE;

    /**
     * URL路径
     */
    String URL = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})(:[\\d]+)?([\\/\\w\\.-]*)*\\/?$";

    /**
     * 身份证校验，初级校验，具体规则有一套算法
     */
    String ID_CARD = "^\\d{15}$|^\\d{17}([0-9]|X)$";

    /**
     * 域名校验
     */
    String DOMAIN = "^[0-9a-zA-Z]+[0-9a-zA-Z\\.-]*\\.[a-zA-Z]{2,4}$";
}
