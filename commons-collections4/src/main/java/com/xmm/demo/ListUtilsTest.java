package com.xmm.demo;

import org.apache.commons.collections4.ListUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xmm
 */
public class ListUtilsTest{
    private static List<Integer> numList1 = new ArrayList<>();
    private static List<Integer> numList2 = new ArrayList<>();
    static {
        numList1.add(2);
        numList1.add(3);
        numList1.add(4);
        numList1.add(5);
        numList1.add(6);
        numList1.add(6);
    }
    static {
        numList2.add(5);
        numList2.add(6);
        numList2.add(7);
        numList2.add(8);
        numList2.add(9);
    }

    public static void main(String[] args) {
        List<List<Integer>> partition = ListUtils.partition(numList2, 2);
        for (List<Integer> list : partition) {
            System.err.println(list);
        }
    }
    private static void retainAll(){
        List<Integer> retainAll = ListUtils.retainAll(numList1, numList2);
        retainAll = retainAll.stream().distinct().collect(Collectors.toList());
        System.err.println(retainAll);
    }
    /**
     * 返回list，该list包含元素存在于list参数1，不存于list参数2，但是只清除一次
     */
    private static void subtract(){
        List<Integer> subtract = ListUtils.subtract(numList1, numList2);
        System.err.println(subtract);
    }

    /**
     * 返回list，该list包含元素存在于list参数1，不存于list参数2
     */
    public static void removeAll(){
        List<Integer> removeAll = ListUtils.removeAll(numList1, numList2);
        System.err.println(removeAll);
    }

    /**
     * 将list按指定大小分割成若干个list，并将多个list添加到一个list2中返回
     */
    private static void partition(){
        List<List<Integer>> partition = ListUtils.partition(numList1, 2);
        System.err.println(partition);
        System.err.println(partition.size());
        System.err.println(partition.get(0));
    }
}
