package com.xmm.demo.java8.lambda.interfaces;

/**
 * @author xmm
 */
public interface ArgsReturn {
    /**
     * 有参有返回值
     * @param one 第一个参数
     * @param two 第二个参数
     * @return 返回String类型数据
     */
    String returnStart(String one, String two);
}
