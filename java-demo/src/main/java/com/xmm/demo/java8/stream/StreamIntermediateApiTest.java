package com.xmm.demo.java8.stream;

import com.xmm.demo.assist.constant.CommonConstant;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Stream流的中间操作
 *
 * @author xmm
 */
public class StreamIntermediateApiTest {
    private List<Integer> numList = CommonConstant.integerList;

    /**
     * filter()方法进行过滤操作
     */
    @Test
    public void filter() {
        Stream<Integer> stream = numList.stream().filter(x -> x < 16);
        stream.forEach(System.out::print);
    }

    /**
     * map()进行元素转换
     */
    @Test
    public void map() {
        Stream<String> stream = numList.stream().map(x -> x + " ");
        stream.forEach(System.out::print);
    }
    @Test
    public void mapToXxx(){
        IntStream intStream = numList.stream().mapToInt(Integer::intValue);
        intStream.forEach(System.out::println);
    }

    /**
     * flatMap()进行流的合并
     */
    @Test
    public void flatMap() {
        ArrayList<List<Integer>> arrayLists = new ArrayList<>();
        arrayLists.add(numList);
        arrayLists.add(numList);
        Stream<Integer> stream = arrayLists.stream().flatMap(List::stream);

    }
    @Test
    public void flatMapToXxx(){
        ArrayList<List<Integer>> arrayLists = new ArrayList<>();
        arrayLists.add(numList);
        arrayLists.add(numList);
        IntStream stream = arrayLists.stream().flatMapToInt(
                list -> list.stream().mapToInt(Integer::intValue));
    }

    /**
     * limit()方法返回包含n个元素的新流
     */
    @Test
    public void limit() {
        Stream<Integer> stream = numList.stream().limit(3);
    }

    /**
     * skip()方法丢弃掉前n个元素的新流，和limit()一起使用可以完成分页的功能
     * stream.skip(pageNumber*pageSize).limit(pageSize)
     */
    @Test
    public void skip() {
        Stream<Integer> stream = numList.stream().skip(3);
    }

    /**
     * distinct()方法去除流中的重复元素
     */
    @Test
    public void distinct() {
        Stream<Integer> stream = numList.stream().distinct();
    }

    /**
     * sorted()方法传入Comparator类型的函数进行排序
     */
    @Test
    public void sorted() {
        Stream<Integer> stream = numList.stream().sorted((x, y) -> y - x);
    }

    /**
     * mapTo[Int|Long|Double] 将流转换为数字流
     */
    @Test
    public void numStream() {
        IntStream intStream = numList.stream().mapToInt(Integer::valueOf);
        LongStream longStream = numList.stream().mapToLong(Long::valueOf);
        DoubleStream doubleStream = numList.stream().mapToDouble(Double::valueOf);
    }

    /**
     * 获取集合中指定范围的元素进行操作
     */
    @Test
    public void range() {
        int index = 5;
        IntStream.range(2, numList.size()).forEach(i -> {
            if (i < index) {
                System.out.println(numList.get(i));
            }
        });
    }
}
