package com.xmm.demo.java8.lambda;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.junit.Test;

import java.util.concurrent.*;
import java.util.function.Function;

/**
 * 方法引用
 * @author xmm
 */
public class MethodsReferenceTest {
    @Test
    public void nonStaticMethodTest(){
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build();
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue(5);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4, 10, 1000L,TimeUnit.MILLISECONDS,workQueue,namedThreadFactory);
        MethodsReferenceTest object = new MethodsReferenceTest();
        Runnable thread = object::nonStaticMethod;
        threadPoolExecutor.execute(thread);
        Function<MethodsReferenceTest, String> function = MethodsReferenceTest::nonStaticMethod;
        String apply = function.apply(new MethodsReferenceTest());
        System.out.println(apply);
        Runnable thread2 = MethodsReferenceTest::staticMethod;
        threadPoolExecutor.execute(thread2);
    }

    public String nonStaticMethod(){
        System.out.println(Thread.currentThread().getName() + "这是实例方法");
        return "success";
    }
    public static void staticMethod(){
        System.out.println(Thread.currentThread().getName() + "这是静态方法");
    }
}
