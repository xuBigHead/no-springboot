package com.xmm.demo.structural.adapter.interfaces;

/**
 * 接口的适配器模式
 *   适用于只想实现Target接口的部分方法，而不是全部方法的情况
 *
 * @author xmm
 * @since 2020/1/17
 */
public abstract class Adapter implements Target{
    @Override
    public void method1() {}
    @Override
    public void method2() {}
}
