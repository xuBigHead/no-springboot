package com.xmm.demo.assist.constant;

import java.util.Arrays;
import java.util.List;

/**
 * 公共常量类
 *
 * @author xmm
 */
public class CommonConstant {
    /**
     * Integer类型的数组
     * <p>{2, 4, 8, 16, 32, 64, 128, 256, 512, 1024}
     */
    public static Integer[] numArray = {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
    /**
     * List{@code <Integer>}类型的集合
     * <p>集合元素为：{2, 4, 8, 16, 32, 64, 128, 256, 512, 1024}
     */
    public static List<Integer> integerList = Arrays.asList(numArray);
}
