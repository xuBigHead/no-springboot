package com.xmm.demo.structural.adapter.interfaces;

/**
 * method1是需要被实现的方法，method2是不需要被实现的方法
 * @author xmm
 * @since 2020/1/17
 */
public interface Target {
    void method1();
    void method2();
}
