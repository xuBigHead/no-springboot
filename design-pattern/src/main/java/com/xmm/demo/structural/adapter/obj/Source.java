package com.xmm.demo.structural.adapter.obj;

/**
 * @author xmm
 * @since 2020/1/17
 */
public class Source {
    public void method1() {
        System.out.println("this is original method!");
    }
}
