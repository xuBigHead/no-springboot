package com.xmm.demo.java8.stream;

import com.xmm.demo.assist.constant.CommonConstant;
import com.xmm.demo.assist.utils.JsonUtils;
import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Stream的collect收集方法的使用
 *
 * @author xmm
 */
public class StreamCollectApiTest {
    private List<Integer> numList = CommonConstant.integerList;

    /**
     * collect方法将流中的元素聚合，可以转换为list、set、map或string
     */
    @Test
    public void collect() {
        List<Integer> list = numList.stream()
                .filter(x -> x > 8)
                .collect(Collectors.toList());
        System.out.println(list);
        Set<Integer> set = numList.stream()
                .filter(x -> x > 8)
                .collect(Collectors.toSet());
        System.out.println(set);
        TreeSet<Integer> treeSet = numList.stream()
                .filter(x -> x > 8)
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(treeSet);
        String string = numList.stream()
                .map(x -> x + " ").collect(Collectors.joining());
        System.out.println(string);
        String stringJoin = numList.stream()
                .map(String::valueOf).collect(Collectors.joining("-","[","]"));
        System.out.println(stringJoin);
    }

    /**
     * 将结果集收集到map中
     */
    @Test
    public void toMap() {
        Map<Integer, Integer> map = numList.stream()
                .collect(Collectors.toMap(Integer::intValue, Integer::intValue));
        System.out.println(map);
        // 已具体的元素来作为生成的map的value
        Map<Integer, Integer> map2 = numList.stream()
                .collect(Collectors.toMap(Integer::intValue, Function.identity()));
        System.out.println(map2);
        // 生成的map的key冲突时，可以通过第三个参数来指定冲突时保留的值
        Map<Integer, Integer> map3 = numList.stream()
                .collect(Collectors.toMap(
                        x -> x.toString().length(),
                        Function.identity(),
                        (nowValue, newValue) -> nowValue));
        System.out.println(map3);
        // 可以通过第四个参数来生成指定的map
        TreeMap<Integer, Integer> map4 = numList.stream()
                .collect(Collectors.toMap(
                        x -> x.toString().length(),
                        Function.identity(),
                        (nowValue, newValue) -> newValue,
                        TreeMap::new));
        System.out.println(map4);
    }

    /**
     * groupingBy()将集合按照指定的函数来分组
     */
    @Test
    public void groupingBy() {
        Map<Integer, List<Integer>> groupMap = numList.stream()
                .collect(Collectors.groupingBy(x -> x.toString().length()));
        System.out.println("groupMap：" + groupMap);
    }

    /**
     * 分类函数返回布尔值的函数时，使用partitioningBy()更有效
     */
    @Test
    public void partitioningBy() {
        Map<Boolean, List<Integer>> partitionMap = numList.stream()
                .collect(Collectors.partitioningBy(x -> x == 16));
        System.out.println(partitionMap);
    }

    /**
     * counting()可以统计不同组的元素个数，适用于groupingBy()和partitioningBy()
     */
    @Test
    public void counting() {
        Map<Integer, Long> countMap = numList.stream()
                .collect(Collectors.groupingBy(
                        x -> x.toString().length(),
                        Collectors.counting()));
        System.out.println(countMap);
        Map<Boolean, Long> countMap2 = numList.stream()
                .collect(Collectors.partitioningBy(
                        x -> x >= 128,
                        Collectors.counting()));
        System.out.println(countMap2);
    }

    /**
     * summingInt方法可以在分组后对每组元素进行求和
     */
    @Test
    public void summingInt() {
        Map<Integer, Integer> sumNumMap = numList.stream()
                .collect(Collectors.groupingBy(x -> x.toString().length(),
                        Collectors.summingInt(x -> x)));
        System.out.println(sumNumMap);
        Map<Boolean, Integer> sumNumMap2 = numList.stream()
                .collect(Collectors.partitioningBy(
                        x -> x >= 128,
                        Collectors.summingInt(x -> x)));
        System.out.println(sumNumMap2);
    }

    /**
     * maxBy方法可以在分组后获取每组的最大值
     */
    @Test
    public void maxBy() {
        Map<Integer, Optional<Integer>> maxMap = numList.stream()
                .collect(Collectors.groupingBy(
                        x -> x.toString().length(),
                        Collectors.maxBy(Comparator.comparing(x -> x))));
        System.out.println(JSON.toJSONString(maxMap));
        Map<Boolean, Optional<Integer>> maxMap2 = numList.stream()
                .collect(Collectors.partitioningBy(
                        x -> x >= 128,
                        Collectors.maxBy(Comparator.comparing(x -> x))));
        System.out.println(JSON.toJSONString(maxMap2));
    }

    /**
     * minBy方法可以在分组后获取每组的最小值
     */
    @Test
    public void minBy() {
        Map<Integer, Optional<Integer>> minMap = numList.stream()
                .collect(Collectors.groupingBy(
                        x -> x.toString().length(),
                        Collectors.minBy(Comparator.comparing(x -> x))));
        System.out.println(JSON.toJSONString(minMap));
        Map<Boolean, Optional<Integer>> minMap2 = numList.stream()
                .collect(Collectors.partitioningBy(
                        x -> x >= 128,
                        Collectors.minBy(Comparator.comparing(x -> x))));
        System.out.println(JSON.toJSONString(minMap2));
    }

    /**
     * mapping方法会将结果应用到另一个收集器上，获取分组中最大值的值
     */
    @Test
    public void mapping(){
        Map<Integer, Optional<Integer>> map = numList.stream()
                .collect(Collectors.groupingBy(
                        x -> x.toString().length(),
                        Collectors.mapping(x -> x,
                                Collectors.maxBy(Comparator.comparing(Integer::valueOf)))));
        System.out.println(JSON.toJSONString(map));
        Map<Boolean, Optional<Integer>> map2 = numList.stream()
                .collect(Collectors.partitioningBy(
                        x -> x >= 128,
                        Collectors.mapping(x -> x,
                                Collectors.maxBy(Comparator.comparing(Integer::valueOf)))));
        System.out.println(JsonUtils.toJSONString(map2));
    }

    /**
     * summarizingInt方法可以将返回类型是int、long、double的元素收集
     * 获取其函数值的总和、平均值、总数、最大值和最小值
     */
    @Test
    public void summarizingInt(){
        IntSummaryStatistics summaryStatistics = numList
                .stream()
                .collect(Collectors.summarizingInt(Integer::intValue));
        System.out.println(summaryStatistics);
        Map<Integer, IntSummaryStatistics> map = numList.stream()
                .collect(Collectors.groupingBy(
                        x -> x.toString().length(),
                        Collectors.summarizingInt(x -> x)
                ));
        System.out.println(JsonUtils.toJSONString(map));
    }
}
