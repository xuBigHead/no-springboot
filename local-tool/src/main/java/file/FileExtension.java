package file;

/**
 * @author xmm
 * @since 2020/2/16
 */
public interface FileExtension {
    String MOBI = ".mobi";
    String AZW3 = ".azw3";
    String EPUB = ".epub";
}
