package com.xmm.demo.structural.adapter.clazz;

/**
 * @author xmm
 * @since 2020/1/17
 */
public interface Target {
    /**
     * 与原类中的方法相同
     */
    void method1();
    /**
     * 新类的方法
     */
    void method2();
}
