package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public class VegBurger extends BaseBurger {
    @Override
    public float price() {
        return 25.0f;
    }

    @Override
    public String name() {
        return "Veg Burger";
    }
}
