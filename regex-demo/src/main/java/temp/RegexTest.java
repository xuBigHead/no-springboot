package temp;

import constant.RegexConstant;
import org.junit.Test;
import utils.RegexUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xmm
 */
public class RegexTest {
    @Test
    public void regexDeptNameTest(){
        //String str = "采贝/一/二/三/四/五/六七八九十一二三四五六七八九十一二三四五六七八九十";
        String str = "采贝/一/二/三/四/五六七八九十一二三四五六七八九十一二三四五六七八九十,采贝/一/二/三/四/五";
        String regex = "^采贝(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5}([，,]采贝(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5})*$";
        System.err.println(RegexUtil.match(regex, str));
    }
    @Test
    public void regexDeptNameTest2(){
        //String str = "采贝/一/二/三/四/五/六七八九十一二三四五六七八九十一二三四五六七八九十";
        String str = "一/二/三/四/五六七八九十一二三四五六七八九十一二三四五六七八九十,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五";
        String regex = "^([a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30})(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5}([，,]([a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30})(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5})*$";
        System.err.println(RegexUtil.match(regex, str));
    }
    @Test
    public void regexDeptNameTest3(){
        //String str = "采贝/一/二/三/四/五/六七八九十一二三四五六七八九十一二三四五六七八九十";
        String str = "一/二/三/四/五六七八九十一二三四五六七八九十一二三四五六七八九十,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五,一/二/三/四/五";
        String regex = "^([a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30})(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5}([，,]([a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30})(/[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,30}){0,5})*$";
        System.err.println(RegexUtil.match(regex, str));
    }

    @Test
    public void idCardTest(){
        String str = "340823199409010210";
        String id = "441424199512313001";
        String regex = "[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]";
        String regex2 = "/^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$/";
        System.err.println(RegexUtil.match(regex2, id));
    }

    @Test
    public void idCardTest2(){
        String str = "3";
        String id = "441424";
        String regex = "[1-9]";
        System.err.println(RegexUtil.match(regex, str));
    }
    @Test
    public void str(){
        String name = "1，2,3，4,5，6,";
        String s = name.replaceAll("，", ",");
        System.err.println(s);
    }

    @Test
    public void list(){
        List<String> list = new ArrayList<>(10);
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        for (String s : list) {
            s = s + "-";
        }
        System.err.println(list);
    }

    @Test
    public void str2(){
        String newDeptId = "1211843045300858882,1211843547220635649";
        String substring = newDeptId.substring(0, 19);
        System.err.println(substring);
    }
    @Test
    public void regexPhoneNumberTest(){
        boolean match = RegexUtil.match(RegexConstant.PHONE, "139068975699");
        System.err.println(match);
    }
    @Test
    public void regexUserNameTest(){
        boolean match = RegexUtil.match(RegexConstant.USER_NAME, "a13906897569");
        System.err.println(match);
    }
    @Test
    public void regexEmailTest(){
        boolean match = RegexUtil.match(RegexConstant.EMAIL, "a13906897569@qq.m");
        System.err.println(match);
    }
    @Test
    public void regexIdCardTest(){
        boolean match = RegexUtil.match(RegexConstant.ID_CARD, "34082319940901021a");
        System.err.println(match);
    }
}
