package utils;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmm
 * @since 2020/1/20
 */
public class MatcherTest {
    private String regex = "\\d+";

    /**
     * pattern()
     * 返回创建Matcher的Pattern对象
     */
    @Test
    public void pattern() {
        Pattern pattern1 = Pattern.compile(regex);
        Matcher matcher = pattern1.matcher("123");
        Pattern pattern2 = matcher.pattern();
        System.err.println(pattern1 == pattern2);
    }

    /**
     * matches()
     * 根据指定的regex来匹配string，等价于Pattern.matcher()方法
     */
    @Test
    public void matches() {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("123");
        boolean b = matcher.matches();
        System.err.println(b);
    }

    /**
     * lookingAt()
     * 对string头部进行匹配，只要匹配到就返回true
     */
    @Test
    public void lookingAt() {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("a1aaa");
        boolean b = matcher.lookingAt();
        System.err.println(b);
    }

    /**
     * find()
     * 对string进行匹配，从下标为0的位置开始，任意位置匹配到即返回true
     * find(int i)
     * 对string进行匹配，从下标为i的位置开始，任意位置匹配到即返回true
     */
    @Test
    public void find() {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher("a1aaa");
        boolean b = matcher.find();
        System.err.println(b);
        boolean b1 = matcher.find(2);
        System.err.println(b1);
    }

    @Test
    public void numGroup() {
        String test = "020-85653333";
        String reg = "(0\\d{2})-(\\d{8})";
        Pattern pattern = Pattern.compile(reg);
        Matcher mc = pattern.matcher(test);
        if (mc.find()) {
            System.out.println("分组的个数有：" + mc.groupCount());
            for (int i = 0; i <= mc.groupCount(); i++) {
                System.out.println("第" + i + "个分组为：" + mc.group(i));
            }
        }
    }

    @Test
    public void nameGroup() {
        String test = "020-85653333";
        String reg = "(?<name1>0\\d{2})-(?<name2>\\d{8})";
        Pattern pattern = Pattern.compile(reg);
        Matcher mc = pattern.matcher(test);
        if (mc.find()) {
            System.out.println("分组的个数有：" + mc.groupCount());
            System.out.println(mc.group("name1"));
            System.out.println(mc.group("name2"));
        }
    }

    @Test
    public void nonGroup() {
        String test = "020-85653333";
        String reg = "(?:0\\d{2})-(\\d{8})";
        Pattern pattern = Pattern.compile(reg);
        Matcher mc = pattern.matcher(test);
        if (mc.find()) {
            System.out.println("分组的个数有：" + mc.groupCount());
            for (int i = 0; i <= mc.groupCount(); i++) {
                System.out.println("第" + i + "个分组为：" + mc.group(i));
            }
        }
    }

    @Test
    public void multiPattern() {
        String test = "aabbbgbdd";
        String reg = "(\\w)\\1";
        Pattern pattern = Pattern.compile(reg);
        Matcher mc = pattern.matcher(test);
        while (mc.find()) {
            System.out.println(mc.group());
        }
    }

    @Test
    public void replaceAll() {
        String test = "abcbbabcbcgbddesddfiid";
        String reg = "(a)(b)c";
        System.out.println(test.replaceAll(reg, "$1"));
    }

    @Test
    public void greedyPattern() {
        String reg = "\\d{3,6}";
        String test = "01234 56 789";
        Pattern p1 = Pattern.compile(reg);
        Matcher m1 = p1.matcher(test);
        while (m1.find()) {
            System.out.println("匹配结果：" + m1.group(0));
        }
    }

    @Test
    public void greedyPattern2() {
        String reg = "(\\d{1,2})(\\d{3,4})";
        String test = "0123456789 01234 0123 01 0";
        Pattern p1 = Pattern.compile(reg);
        Matcher m1 = p1.matcher(test);
        while (m1.find()) {
            System.out.println("匹配结果：" + m1.group(0));
        }
    }

    @Test
    public void lazyPattern() {
        String reg = "(\\d{1,2}?)(\\d{3,4})";
        String test = "0123 4 56789";
        Pattern p1 = Pattern.compile(reg);
        Matcher m1 = p1.matcher(test);
        while (m1.find()) {
            System.out.println("匹配结果：" + m1.group(0));
        }
    }
}
