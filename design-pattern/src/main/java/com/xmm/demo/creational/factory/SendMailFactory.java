package com.xmm.demo.creational.factory;

/**
 * @author xmm
 * @since 2020/1/19
 */
public class SendMailFactory implements Provider{
    public static SendMailFactory build() {
        return new SendMailFactory();
    }
    @Override
    public Sender produce() {
        return new MailSender();
    }
}
