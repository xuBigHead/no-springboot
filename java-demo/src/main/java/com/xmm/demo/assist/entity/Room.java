package com.xmm.demo.assist.entity;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author xmm
 */
@Data
public class Room {
    Integer high;
    Integer width;
    Integer length;
    public Room(){}
    public Room(Integer high, Integer width, Integer length) {
        System.out.println("build room");
        this.high = high;
        this.width = width;
        this.length = length;
    }

    public static List<Room> getRoomList() {
        return Lists.newArrayList(
                new Room(1, 1, 2),
                new Room(2, 2, 4),
                new Room(2, 3, 8),
                new Room(3, 4, 16),
                new Room(3, 5, 32),
                new Room(3, 6, 64));
    }
}
