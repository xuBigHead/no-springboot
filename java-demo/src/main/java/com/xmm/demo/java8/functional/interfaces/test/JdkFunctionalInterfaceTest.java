package com.xmm.demo.java8.functional.interfaces.test;

import org.junit.Test;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * Java8提供的四大函数式接口的使用
 * functional interfaces
 * @author xmm
 */
public class JdkFunctionalInterfaceTest {
    /**
     * 消费型接口 Consumer<T>
     *     void accept(T t)
     *         接收一个参数进行消费，但无需返回结果。
     *     default Consumer<T> andThen(Consumer<? super T> after)
     *         c1.andThen(c2).apply(arg)，c1消费完arg后，再将arg传给c2消费。
     */
    @Test
    public void consumerDemo(){
        // void accept(T t)
        Consumer<String> consumer = e -> System.out.println(e.substring(3,6));
        consumer.accept("123456");
        // default Consumer<T> andThen(Consumer<? super T> after)
        Consumer<String> consumer2 = e -> System.out.println(e.substring(2,5));
        consumer.andThen(consumer2).accept("123456");
    }

    /**
     * 供给型接口 Supplier<T>
     *     T get()
     *         返回一个自定义数据
     */
    @Test
    public void supplierDemo(){
        // T get()
        Supplier<String> supplier = () -> new Random().nextInt(100) + "";
        System.out.println(supplier.get());
    }
    /**
     * 函数型接口 Function<T,R>
     *     R apply(T t)：
     *         传入一个参数，返回想要的结果。
     *     default <V> Function<V, R> compose(Function<? super V, ? extends T> before)
     *         f1.compose(f2).apply(arg)，表示先执行f2，然后将得到的结果传给f1执行。
     *     default <V> Function<T,V> andThen(Function<? super R,? extends V> after)
     *         f1.andThen(f2).apply(arg)，表示先执行f1，然后将得到的结果传给f2执行。
     *     static <T> Function<T, T> identity()
     *         获取到一个输入参数和返回结果一样的Function实例
     */
    @Test
    public void functionDemo(){
        // R apply(T t)
        Function<Integer,String> integerToString = (x) -> "数值为：" + x;
        String string1 = integerToString.apply(new Random().nextInt(100));
        System.out.println(string1);
        // default <V> Function<V, R> compose(Function<? super V, ? extends T> before)
        Function<String,Integer> stringToInteger = Integer::valueOf;
        String string2 =
                integerToString
                        .compose(stringToInteger).apply(new Random().nextInt(100) + "");
        System.out.println(string2);
        // default <V> Function<T,V> andThen(Function<? super R,? extends V> after)
        String string3 =
                stringToInteger
                        .andThen(integerToString).apply(new Random().nextInt(100) + "");
        System.out.println(string3);
        // static <T> Function<T, T> identity()
        Function<String, String> stringToString = Function.identity();
        String string4 = stringToString.apply("myself");
        System.out.println(string4);
    }
    /**
     * 断言型接口 Predicate<T>
     *     boolean test(T t)
     *         传入一个参数，返回一个布尔值
     *     default Predicate<T> negate()
     *         表示 ! p1.test()
     *     default Predicate<T> and(Predicate<? super T> other)
     *         p1.and(p2).test(arg)，表示p1.test(arg) && p2.test(arg)
     *     default Predicate<T> or(Predicate<? super T> other)
     *         p1.or(f2).test(arg)，表示p1.test(arg) || p2.test(arg)
     *     static <T> Predicate<T> isEqual(Object targetRef)
     *         获取到一个Predicate实例p，p.test(arg) 表示targetRef 是否等于arg
     */
    @Test
    public void predicateDemo(){
        // boolean test(T t)
        Predicate<String> predicate = str -> str.length() > 2;
        System.out.println(predicate.test("hello"));
        // default Predicate<T> negate()
        Predicate<String> negate = predicate.negate();
        System.out.println(negate.test("hello"));
        // default Predicate<T> and(Predicate<? super T> other)
        Predicate<String> predicateAndNegate = predicate.and(negate);
        System.out.println(predicateAndNegate.test("hello"));
        // default Predicate<T> or(Predicate<? super T> other)
        Predicate<String> predicateOrNegate = predicate.or(negate);
        System.out.println(predicateOrNegate.test("hello"));
        // static <T> Predicate<T> isEqual(Object targetRef)
        System.out.println(
                Predicate
                        .isEqual(new Random().nextInt(2))
                        .test(new Random().nextInt(2))
        );
    }
}
