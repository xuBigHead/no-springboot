package com.xmm.demo.structural.adapter.interfaces;

/**
 *
 * @author xmm
 * @since 2020/1/17
 */
public class TargetImpl extends Adapter{
    @Override
    public void method1(){
        System.out.println("first");
    }

    public static void main(String[] args) {
        Target targetImpl = new TargetImpl();
        targetImpl.method1();
        // 不需要被实现的方法
        targetImpl.method2();
    }
}
