package com.xmm.demo.structural.adapter.clazz;

/**
 * 类的适配器模式
 *   通过适配器将Source类的方法扩展到Target接口的实现类中
 *   适用于Source没有实现Target接口，又想要在Target实现类中使用Source类中的方法的情况
 * @author xmm
 * @since 2020/1/17
 */
class Adapter extends Source implements Target{
    /**
     * Target接口的实现类就具有了 Source 类的功能。
     */
    @Override
    public void method2() {
        method1();
        System.out.println("this is the target method!");
    }

    public static void main(String[] args) {
        Target target = new Adapter();
        target.method1();
        target.method2();
    }
}

