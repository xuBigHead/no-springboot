package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "Bottle";
    }
}
