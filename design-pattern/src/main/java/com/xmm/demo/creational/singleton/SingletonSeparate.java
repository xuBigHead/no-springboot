package com.xmm.demo.creational.singleton;

/**
 * 将对象的创建和获取分开，单独为创建添加synchronized关键字
 *
 * @author xmm
 * @since 2020/1/20
 */
public class SingletonSeparate {
    private static SingletonSeparate instance = null;
    private SingletonSeparate() {
    }
    public static synchronized void syncInit() {
        if(instance == null) {
            instance = new SingletonSeparate();
        }
    }
    public static SingletonSeparate getInstance() {
        if(instance == null) {
            syncInit();
        }
        return instance;
    }
}
