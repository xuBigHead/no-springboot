package utils;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmm
 * @since 2020/1/20
 */
public class PatternTest {
    private String regex = "\\d+";
    /**
     * pattern()
     *  返回compile的参数
     */
    @Test
    public void pattern(){
        Pattern pattern = Pattern.compile(regex);
        String regexStr = pattern.pattern();
        System.err.println(regexStr);
    }

    /**
     * pattern.split(CharSequence input)
     *  根据指定的regex来分割string，并返回string数组，数组长度不限
     * pattern.split(CharSequence input, int limit)
     *  根据指定的regex来分割string，并返回string数组，数组长度不能超过limit
     */
    @Test
    public void spilt(){
        Pattern pattern = Pattern.compile(regex);
        String[] strArrays = pattern.split("h1e2l3l4o");
        for (String str : strArrays) {
            System.err.println("no limit : " + str);
        }
        String[] strArrays2 = pattern.split("h1e2l3l4o", 2);
        for (String str : strArrays2) {
            System.err.println("limit : " + str);
        }
    }

    /**
     * Pattern.matches(String regex, CharSequence input)
     *  快速匹配string，适用于只匹配一次，且匹配全部string
     */
    @Test
    public void matcher(){
        boolean matches = Pattern.matches(regex, "1234a");
        System.err.println(matches);
    }
}
