package com.xmm.demo.java8.stream;

import com.xmm.demo.assist.constant.CommonConstant;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Stream流的创建操作
 * @author xmm
 */
public class StreamBuildApiTest {
    private Integer[] numArray = CommonConstant.numArray;
    private List<Integer> numList = CommonConstant.integerList;

    @Test
    public void build() {
        // 最常用集合的stream()来创建流
        Stream<Integer> stream1 = numList.stream();
        Stream<Integer> stream2 = Arrays.stream(numArray);
        Stream<List<Integer>> stream3 = Stream.of(this.numList);
        Stream<Integer> stream4 = Stream.of(this.numArray);
        // 产生无限流
        Stream<String> stream5 = Stream.generate(() -> "test");
        Stream<Integer> stream6 = Stream.iterate(0, n -> n + 10);
        Stream<Integer> stream7 = StreamSupport.stream(numList.spliterator(), false);
    }

    /**
     * parallel()方法可以将任意的串行流转换为一个并行流
     * 注意要确保传递给并行流操作的函数是线程安全的
     */
    @Test
    public void parallel(){
        Stream<Integer> parallelStream = numList.parallelStream();
        Stream<Integer> parallelStream2 = numList.stream().parallel();
        parallelStream.forEach(x -> System.out.print(x + "-"));
        System.out.println();
        numList.forEach(x -> System.out.print(x + "-"));
    }
}
