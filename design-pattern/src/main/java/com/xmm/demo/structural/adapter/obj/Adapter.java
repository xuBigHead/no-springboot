package com.xmm.demo.structural.adapter.obj;

/**
 * 对象的适配器模式
 *   对象的适配器模式不是继承Source类而是将Source类的对象作为属性来使用Source类中的方法
 *   适用于Source没有实现Target接口，又想要在Target实现类中使用Source类中的方法的情况
 * @author xmm
 * @since 2020/1/17
 */
public class Adapter implements Target {
    private Source source;
    Adapter(Source source) {
        super();
        this.source = source;
    }

    public static void main(String[] args) {
        Source source = new Source();
        Target target = new Adapter(source);
        target.method1();
        target.method2();
    }

    @Override
    public void method1() {
        source.method1();
    }
    @Override
    public void method2() {
        System.out.println("this is the target method!");
    }
}
