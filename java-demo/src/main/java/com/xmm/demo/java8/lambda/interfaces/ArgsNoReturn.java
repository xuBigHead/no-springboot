package com.xmm.demo.java8.lambda.interfaces;

/**
 * @author xmm
 */
public interface ArgsNoReturn {
    /**
     * 有参无返回值
     * @param one 第一个参数
     * @param two 第二个参数
     */
    void start(String one, String two);
}
