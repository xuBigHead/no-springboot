package com.xmm.demo.java8.functional.interfaces.test;

import com.xmm.demo.java8.functional.interfaces.diy.FunctionalInterface;
import org.junit.Test;

/**
 * 自定义函数式接口使用
 * @author xmm
 */
public class DiyFunctionalInterfaceTest {
    /**
     * 使用函数式接口的抽象方法
     */
    @Test
    public void abstractMethodTest(){
        FunctionalInterface<String> functionalInterface = System.out::println;
        functionalInterface.method("test");
    }

    /**
     * 使用函数式接口的默认方法
     */
    @Test
    public void defaultMethodTest(){
        FunctionalInterface functionalInterface = System.out::println;
        functionalInterface.defaultMethod();
    }

    /**
     * 使用函数式接口的静态方法
     */
    @Test
    public void staticMethodTest(){
        FunctionalInterface.staticMethod();
    }
}
