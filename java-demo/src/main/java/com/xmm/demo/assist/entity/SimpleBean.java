package com.xmm.demo.assist.entity;

import lombok.Data;

/**
 * @author xmm
 */
@Data
public class SimpleBean {
    Long id;
    String simpleBeanName;
}
