package com.xmm.demo.structural.decorator;

/**
 * @author xmm
 * @since 2020/1/17
 */
public interface Sourceable {
    void method();
}
