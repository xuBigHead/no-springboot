package com.xmm.demo.creational.factory;

/**
 * 工厂模式还可以和接口的适配器模式结合，通过抽象类的方式来作为中间层，避免实现Provider所有方法
 *
 * @author xmm
 * @since 2020/1/19
 */
public class SendSmsFactory implements Provider{
    public static SendSmsFactory build() {
        return new SendSmsFactory();
    }
    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
