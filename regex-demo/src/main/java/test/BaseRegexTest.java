package test;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmm
 * @since 2020/1/21
 */
public class BaseRegexTest {
    @Test
    public void baseCharRegex() {
        //注意：\r,\n,\b等转义字符在java字符串常量中要写成\\r,\\n,\\b等，否则编译都过不去
        // \s匹配\r,\n,\r和空格
        System.out.println(" \t\n\r".matches("\\s{4}"));
        // \S和\s互逆
        System.out.println("/".matches("\\S"));
        // .不匹配\r和\n
        System.out.println("\r".matches("."));
        System.out.println("\n".matches("."));
        // \w匹配字母，数字和下划线
        System.out.println("a8_".matches("\\w\\w\\w"));
        // \W和\w互逆
        System.out.println("&_".matches("\\W\\w"));
        // \d匹配数字
        System.out.println("8".matches("\\d"));
        // \D与\d互逆
        System.out.println("%".matches("\\D"));
    }

    @Test
    public void lineTerminator() {
        // 两者都匹配但意文不同，表示\000a匹配\000a
        System.out.println("\n".matches("\n"));
        // 表示\n匹配换行
        System.out.println("\n".matches("\\n"));
        System.out.println("\r".matches("\r"));
        System.out.println("\r".matches("\\r"));
    }

    @Test
    public void baseRegex() {
        System.out.println("======================");
        //^匹配开头
        System.out.println("hell".matches("^hell"));
        System.out.println("abc\nhell".matches("^hell"));
        //$匹配结束
        System.out.println("my car\nabc".matches(".*ar$"));
        System.out.println("my car".matches(".*ar$"));
        //\b匹配界
        System.out.println("bomb".matches("\\bbom."));
        System.out.println("bomb".matches(".*mb\\b"));
        //\B与\b互逆
        System.out.println("abc".matches("\\Babc"));
    }

    @Test
    public void rangeRegex() {
        // 范围匹配，[a-z]匹配a到z的小写字母
        System.out.println("s".matches("[a-z]"));
        System.out.println("S".matches("[A-Z]"));
        System.out.println("9".matches("[0-9]"));
    }

    @Test
    public void negationRegex() {
        // 取反
        System.out.println("s".matches("[^a-z]"));
        System.out.println("S".matches("[^A-Z]"));
        System.out.println("9".matches("[^0-9]"));
    }

    @Test
    public void bracketsRegex() {
        // 括号的作用
        System.out.println("aB9".matches("[a-z][A-Z][0-9]"));
        System.out.println("aB9bC6".matches("([a-z][A-Z][0-9])+"));
    }

    @Test
    public void union() {
        // 或运算
        System.out.println("two".matches("two|to|2"));
        System.out.println("to".matches("two|to|2"));
        System.out.println("2".matches("two|to|2"));
        // [a-zA-z]==[a-z]|[A-Z]
        System.out.println("a".matches("[a-zA-Z]"));
        System.out.println("A".matches("[a-zA-Z]"));
        System.out.println("a".matches("[a-z]|[A-Z]"));
        System.out.println("A".matches("[a-z]|[A-Z]"));
        // 下面四个regex都省略了或符号"|"
        System.out.println(")".matches("[a-zA-Z)]"));
        System.out.println(")".matches("[a-zA-Z)_-]"));
        System.out.println("_".matches("[a-zA-Z)_-]"));
        System.out.println("-".matches("[a-zA-Z)_-]"));
        // "abc"中间省略了或符号"|"，完整regex为"a|b|c"
        System.out.println("b".matches("[abc]"));
        //[a-d[f-h]]==[a-df-h]
        System.out.println("h".matches("[a-d[f-h]]"));
    }

    @Test
    public void intersectionRegex() {
        // 取交集
        System.out.println("a".matches("[a-z&&[def]]"));
        System.out.println("e".matches("[a-z&&[def]]"));
        System.out.println("e".matches("[[a-z]&&[e]]"));
    }

    @Test
    public void subtractionRegex() {
        // 取并
        System.out.println("9".matches("[[a-c][0-9]]"));
        //[a-z&&[^bc]]==[ad-z]
        System.out.println("b".matches("[a-z&&[^bc]]"));
        System.out.println("d".matches("[a-z&&[^bc]]"));
        //[a-z&&[^m-p]]==[a-lq-z]
        System.out.println("d".matches("[a-z&&[^m-p]]"));
        System.out.println("a".matches("\\p{Lower}"));
    }

    @Test
    public void borderRegex() {
        // 注意以下体会\b的用法
        System.out.println("aawordaa".matches(".*\\bword\\b.*"));
        System.out.println("a word a".matches(".*\\bword\\b.*"));
        System.out.println("aawordaa".matches(".*\\Bword\\B.*"));
        System.out.println("a word a".matches(".*\\Bword\\B.*"));
        System.out.println("a word a".matches(".*word.*"));
        System.out.println("aawordaa".matches(".*word.*"));
    }

    /**
     * 该regex表示\\1 引用第一个组
     */
    @Test
    public void refGroup() {
        String regex = "(\\d)\\1";
        Pattern pattern = Pattern.compile(regex);
        // \1表示引用第一个组\n表示引用第n个组(必须用\\1而不能用\1因\1在字符串中另有意义)
        Matcher matcher = pattern.matcher("3344455667788");
        System.out.println(matcher.find());
        System.out.println(matcher.group());
        System.out.println(matcher.find());
        System.out.println(matcher.group());
        System.out.println(matcher.find());
        System.out.println(matcher.group());
        System.out.println(matcher.find(2));
        System.out.println(matcher.group());
        System.out.println("组0：" + matcher.group(0));
        System.out.println("组1：" + matcher.group(1));
        matcher.reset();
        System.out.println(matcher.find());
        System.out.println("组0：" + matcher.group(0));
        System.out.println("组1：" + matcher.group(1));
    }

    /**
     * Greedy quantifiers
     * Greedy quantifiers是最常用的一种，如上，它的匹配方式是先匹配尽可能多的字符，
     * 当这样造成整个表达式整体不能匹配时就退一个字符再试
     * 比如：.*foo与xfooxxxxxxfoo的匹配过程，.*先与整个输入匹配，发现这样不行，整个串不能匹配
     * 于是退最后一个字符"o"再试，还不行，再退直到把foo都退出才发现匹配于是结束。
     * 因为这个过程总是先从最大匹配开始到找到一个匹配，所以.*与之匹配的总是一个最大的
     * 这个特点和资本家相似故名贪婪的
     */
    @Test
    public void greedyQuantifiers() {
        String regex = ".*foo";
        Pattern pattern = Pattern.compile(regex);
        quantifiers(pattern);
    }

    /**
     * Reluctant quantifiers
     * Reluctant quantifiers总是先从最小匹配开始，如果这时导致整个串匹配失败则再吃进一个字符再试
     * 如：.*?foo与xfooxxxxxxfoo的匹配过程，首先.*与空串匹配，这时整个串匹配失败，于是再吃一个x
     * 这时整个串匹配成功，当再调用find时从上次匹配结束时开始找，先吃一个空串，不行，再吃一个x
     * 不行，……直到把中间所有x都吃掉才发现匹配成功
     * 这种方式总是从最小匹配开始所以它能找到最多次数的匹配，但第一匹配都是最小的
     * 它的行为有点象雇佣工人，总是尽可能少的于活，故名勉强的
     */
    @Test
    public void reluctantQuantifiers() {
        String regex = ".*?foo";
        Pattern pattern = Pattern.compile(regex);
        quantifiers(pattern);
    }

    /**
     * Possessive quantifiers
     * Possessive quantifiers这种匹配方式与Greedy方式相似，所不同的是它不够聪明
     * 当它一口吃掉所有可以吃的字符时发现不匹配则认为整个串都不匹配，它不会试着吐出几个
     * 它的行为和大地主相似，贪婪但是愚蠢，所以名曰强占的
     */
    @Test
    public void possessiveQuantifiers() {
        String regex = ".*+foo";
        Pattern pattern = Pattern.compile(regex);
        quantifiers(pattern);
    }

    private void quantifiers(Pattern pattern) {
        String string = "xfooxxxxxxfoo";
        Matcher matcher = pattern.matcher(string);
        boolean isEnd = true;
        int k = 0;
        while (isEnd) {
            try {
                System.out.println("the:" + k++);
                System.out.println(matcher.find());
                System.out.println(matcher.end());
            } catch (Exception e) {
                isEnd = false;
            }
        }
    }
}
