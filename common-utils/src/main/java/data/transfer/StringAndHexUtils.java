package data.transfer;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * 字符串和16进制数据互相转换
 * @author xmm
 */
public class StringAndHexUtils {
    public static void main(String[] args) {
        String str = "a";
        String str2HexStr = str2HexStr(str);
        System.out.println(str2HexStr);
        String hexStr2Str = hexStr2Str(str2HexStr);
        System.out.println(hexStr2Str);

    }
    @Test
    public void decodeX() throws UnsupportedEncodingException {
        String s = "<html>";
        String s1 = s.replaceAll("\\\\x", "%");
        String decode = URLDecoder.decode(s1, "utf-8");
        System.out.println(decode);
    }
    /**
     * 字符串转换成为16进制(无需Unicode编码)
     * @param str 被转换普通字符串
     * @return 16进制字符串
     */
    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder();
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
        }
        return sb.toString().trim();
    }

    /**
     * 16进制直接转换成为字符串(无需Unicode解码)
     * @param hexStr 被转换16进制字符串
     * @return 普通字符串
     */
    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }
}