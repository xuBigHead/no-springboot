package com.xmm.demo.java8.functional.interfaces.diy;

/**
 * 函数式接口的定义
 *      1、只能有一个抽象方法
 *      2、可以有静态方法和默认方法，因为这两种方法都是已经实现的了
 *      3、可以包含Object里所有能重写的方法，因为即使接口包含像String toString()这样的抽象
 *      方法，它的实现类也会因继承了Object类，而再次对接口中的toString()方法进行实现。
 * @author xmm
 */
public interface FunctionalInterface<T> {
    /**
     * 函数式接口只有一个抽象方法
     * @param t 泛型参数
     */
    void method(T t);

    /**
     * 函数式接口可以有实现的默认方法
     */
    default void defaultMethod(){
        System.out.println("this is defaultMethod");
    }

    /**
     * 函数式接口可以有实现的静态方法
     */
    static void staticMethod(){
        System.out.println("this is staticMethod");
    }
}
