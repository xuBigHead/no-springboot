package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public class Coke extends BaseColdDrink {
    @Override
    public float price() {
        return 30.0f;
    }

    @Override
    public String name() {
        return "Coke";
    }
}
