package com.xmm.demo.creational.factory;

/**
 * @author xmm
 * @since 2020/1/19
 */
public class SendFactory {
    public static String MAIL = "mail";
    public static String SMS = "sms";

    /**
     * 通过分支结构来对输入的内容进行判断，返回不同的类的对象
     * 适用于通过参数来决定创建的对象
     */
    public Sender produce(String type) {
        if (MAIL.equals(type)) {
            return new MailSender();
        } else if (SMS.equals(type)) {
            return new SmsSender();
        } else {
            System.err.println("please input correct type!");
            return null;
        }
    }

/**
 * 通过多个方法来返回不同类的对象，各个方法之间不会冲突，从而创建多个类的对象
 * 适用于不需要参数，自行选择需要创建的对象
 *
 * 可以通过将produceMail和produceSms方法设置为静态方法来进一步优化工厂模式
 */
    public Sender produceMail() {
        return new MailSender();
    }

    public Sender produceSms() {
        return new SmsSender();
    }

/**
 * 通过多个静态方法来返回不同类的对象
 * 适用于不需要参数，自行选择需要创建的对象
 */
    public static Sender produceStaticMail() {
        return new MailSender();
    }

    public static Sender produceStaticSms() {
        return new SmsSender();
    }
}
