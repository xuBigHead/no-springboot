package file;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * @author xmm
 * @since 2020/2/16
 */
public class FileOperation {
    public static void main(String[] args) {
        String path = "G:\\Book\\Z";
        FileOperation file = FileOperation.build();
        file.deleteDuplicateFile(path);
    }

    private static FileOperation build() {
        return new FileOperation();
    }

    /**
     * 删除指定顺序的文件，优先保留mobi，其次epub
     *
     * @param path 文件路径
     */
    private void deleteDuplicateFile(String path) {
        File dir = new File(path);
        if (!dir.isDirectory()) {
            System.err.println("请选择文件夹");
        } else {
            String[] files = dir.list();
            if (files == null || files.length == 0) {
                System.err.println("该文件夹下没有文件");
            } else {
                Set<String> fileNames = new HashSet<>(10);
                for (String fileName : files) {
                    int index = fileName.lastIndexOf(".");
                    String fileNameWithoutExtension = fileName.substring(0, index);
                    String fileExtension = fileName.substring(index);
                    if (fileNames.contains(fileNameWithoutExtension)) {
                        if (FileExtension.MOBI.equals(fileExtension)) {
                            deleteFileOfExtension(new File(path + "/" + fileNameWithoutExtension + FileExtension.EPUB));
                        }
                        deleteFileOfExtension(new File(path + "/" + fileNameWithoutExtension + FileExtension.AZW3));
                    } else {
                        fileNames.add(fileNameWithoutExtension);
                    }
                }
            }
        }
    }

    private void deleteFileOfExtension(File file) {
        if (file.exists()) {
            System.out.print("删除" + file.getName());
            if (file.delete()) {
                System.out.println("成功");
            } else {
                System.err.println(file.getName() + "删除失败");
            }
        }
    }
}
