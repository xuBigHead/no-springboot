package com.xmm.demo.creational.builder;

/**
 * @author xmm
 * @since 2020/1/21
 */
public interface Item {
    String name();
    Packing packing();
    float price();
}
