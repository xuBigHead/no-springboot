package com.xmm.demo.structural.adapter.obj;

/**
 * method1为Source类的方法，method2为想要扩展的方法
 * @author xmm
 * @since 2020/1/17
 */
public interface Target {
    void method1();
    void method2();
}
