package com.xmm.demo.assist.utils;

import com.alibaba.fastjson.JSON;

/**
 * JSON工具类
 *
 * @author xmm
 */
public class JsonUtils {
    public static String toJSONString(Object o) {
        return responseFormat(JSON.toJSONString(o));
    }

    /**
     * 控制台中格式化打印JSON字符串
     *
     * @param resString 未格式化JSON字符串
     * @return 格式化JSON字符串
     */
    public static String responseFormat(String resString) {
        StringBuffer jsonForMatStr = new StringBuffer();
        int level = 0;
        //将字符串中的字符逐个按行输出
        for (int index = 0; index < resString.length(); index++) {
            //获取s中的每个字符
            char c = resString.charAt(index);
            //level大于0并且jsonForMatStr中的最后一个字符为\n,jsonForMatStr加入\t
            if (level > 0 && '\n' == jsonForMatStr.charAt(jsonForMatStr.length() - 1)) {
                jsonForMatStr.append(getLevelStr(level));
            }
            //遇到"{"和"["要增加空格和换行，遇到"}"和"]"要减少空格，以对应，遇到","要换行
            switch (c) {
                case '{':
                case '[':
                    jsonForMatStr.append(c + "\n");
                    level++;
                    break;
                case ',':
                    jsonForMatStr.append(c + "\n");
                    break;
                case '}':
                case ']':
                    jsonForMatStr.append("\n");
                    level--;
                    jsonForMatStr.append(getLevelStr(level));
                    jsonForMatStr.append(c);
                    break;
                default:
                    jsonForMatStr.append(c);
                    break;
            }
        }
        return jsonForMatStr.toString();
    }

    private static String getLevelStr(int level) {
        StringBuffer levelStr = new StringBuffer();
        for (int levelI = 0; levelI < level; levelI++) {
            levelStr.append("\t");
        }
        return levelStr.toString();
    }
}
