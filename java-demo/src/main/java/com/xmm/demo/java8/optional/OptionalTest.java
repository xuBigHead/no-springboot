package com.xmm.demo.java8.optional;

import com.xmm.demo.assist.entity.Room;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Optional类的使用
 *
 * @author xmm
 */
public class OptionalTest {
    private Room emptyRoom = null;
    private Room room = new Room(1, 2, 3);

    /**
     * empty方法创建一个空的Optional对象
     */
    @Test(expected = NoSuchElementException.class)
    public void empty() {
        Optional<Room> empty = Optional.empty();
        System.out.println(empty);
    }

    /**
     * Optional类的of和ofNullable方法的区别在于of方法中传入null会抛出空指针异常，
     * 而ofNullable则不会，所以不确定对象是否为null时尽量使用ofNullable方法
     */
    @Test
    public void of() {
        Optional<Room> room = Optional.of(this.room);
        Optional<Room> emptyRoom = Optional.of(this.emptyRoom);
    }

    @Test
    public void ofNullable() {
        Optional<Room> room = Optional.ofNullable(this.room);
        Optional<Room> emptyRoom = Optional.ofNullable(this.emptyRoom);
    }

    /**
     * ifPresent方法可以判断传入Optional中的对象是否不为空，为空则返回false，反之返回true
     */
    @Test
    public void ifPresent() {
        Optional<Room> room = Optional.ofNullable(this.room);
        boolean notNull = room.isPresent();
        System.out.println(notNull);
    }

    /**
     * get方法可以获取传入Optional实例中的对象，但是如果该对象为null时会抛出空指针异常
     */
    @Test
    public void get() {
        Optional<Room> room = Optional.ofNullable(this.room);
        Room newRoom = room.get();
        System.out.println(newRoom);
    }

    /**
     * orElse方法也可以获取该对象，和get不同的是该对象如果为空时，则返回该方法的参数对象(默认值)
     */
    @Test
    public void orElse() {
        Optional<Room> room = Optional.ofNullable(this.room);
        Room newRoom = room.orElse(new Room(4, 5, 6));
        System.out.println(newRoom);
    }

    /**
     * orElseGet方法和orElse方法的区别在于当该对象不为空时，orElse方法仍然会创建参数中的
     * 对象，而orElseGet方法则不会再创建参数中的对象，因此会有微小的性能差异
     */
    @Test
    public void orElseGet(){
        Optional<Room> room = Optional.ofNullable(this.room);
        Room newRoom = room.orElseGet(() -> new Room(4,5,6));
        System.out.println(newRoom);
    }

    /**
     * orElseThrow方法在该对象为null时会抛出指定的异常
     */
    @Test
    public void orElseThrow(){
        Optional<Room> room = Optional.ofNullable(this.emptyRoom);
        Room newRoom = room.orElseThrow(ArrayIndexOutOfBoundsException::new);
        System.out.println(newRoom);
    }

    /**
     * map方法对对象进行转换并将转换后的对象包装在Optional中
     */
    @Test
    public void map(){
        Integer integer = Optional.ofNullable(this.room)
                .map(Room::getLength).orElse(0);
        System.out.println(integer);
    }

    /**
     * flatMap可以将Function函数式接口作为参数来转换为指定的Optional封装的对象
     */
    @Test
    public void flatMap(){
        Integer integer = Optional.ofNullable(this.room)
                .flatMap(x -> Optional.ofNullable(x.getLength()))
                .orElse(0);
        System.out.println(integer);
    }

    /**
     * filter方法接收一个Predicate函数式接口作为参数来实现过滤功能，该函数返回的是true则
     * 正常返回，如果该函数返回的是false则返回一个空的Optional
     */
    @Test
    public void filter(){
        Room room = Optional.ofNullable(this.room)
                .filter(x -> x.getLength() > 10)
                .orElse(new Room());
        System.out.println(room);
    }

}
