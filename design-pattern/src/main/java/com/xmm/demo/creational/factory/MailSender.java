package com.xmm.demo.creational.factory;

/**
 * @author xmm
 * @since 2020/1/19
 */
public class MailSender implements Sender {
    @Override
    public void send() {
        System.err.println("this is mailSender");
    }
}
