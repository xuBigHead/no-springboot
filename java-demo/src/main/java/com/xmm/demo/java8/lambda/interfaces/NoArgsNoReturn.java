package com.xmm.demo.java8.lambda.interfaces;

/**
 * @author xmm
 */
public interface NoArgsNoReturn {
    /**
     * 无参无返回值
     */
    void start();
}
