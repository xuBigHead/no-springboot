package com.xmm.demo.java8.lambda.interfaces;

/**
 * @author xmm
 */
public interface NoArgsReturn {
    /**
     * 无参有返回值
     * @return 返回String类型数据
     */
    String returnStart();
}
